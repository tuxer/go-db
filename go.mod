module gitlab.com/tuxer/go-db

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	gitlab.com/tuxer/go-logger v0.0.0-20200319070532-7f9fc4a4cd75 // indirect
)
